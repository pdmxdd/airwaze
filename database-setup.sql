DROP DATABASE gis;
CREATE DATABASE airwaze;
GRANT ALL PRIVILEGES ON DATABASE airwaze TO airwaze_user;
\c airwaze;
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION postgis_tiger_geocoder;
