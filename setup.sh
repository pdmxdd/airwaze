#!/bin/bash
docker build -t airwaze-db .

docker stop airwaze-psql
docker rm airwaze-psql

docker run --name airwaze-psql -p 5432:5432 -dt airwaze-db

sleep 5

PGPASSWORD=airwazepass psql -h 127.0.0.1 -U airwaze_user -d postgres -f database-setup.sql
