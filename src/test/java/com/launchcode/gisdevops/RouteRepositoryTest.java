package com.launchcode.gisdevops;

import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.Route;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class RouteRepositoryTest {

    //TODO: remove this empty test after writing your own
    @Test
    public void emptyTest() {

    }

//    @Autowired
//    private RouteRepository routeRepository;
//
//    @Autowired
//    private EntityManager entityManager;
//
//    @Test
//    public void findBySrcReturnsMatchgingSrc() {
//        Route route = new Route(WktHelper.wktToGeometry("LINESTRING(45.554194 -122.686101, 45.433001 -122.762632)"), "NZ", 1, "STL", 4, "RAR", 4);
//        entityManager.persist(route);
//        entityManager.flush();
//
//        List<Route> foundRoute = routeRepository.findBySrc(route.getSrc());
//
//        assertEquals(1, foundRoute.size());
//
//    }
}
